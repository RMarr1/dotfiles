# uncomment to debug slow zsh startup time
# zmodload zsh/zprof

# enable command auto-correction.
ENABLE_CORRECTION="true"

# Enable bare string directory switching
setopt AUTO_CD
setopt AUTO_PUSHD

# Disable case sensitivity for tab completion
autoload -Uz compinit
zcompdump="$HOME/.zcompdump"
if [[ -f $zcompdump && (! -f ${zcompdump}.zwc || $zcompdump -nt ${zcompdump}.zwc) ]]; then
    compinit -d $zcompdump
    zcompile $zcompdump
else
    compinit -C
fi
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'

# history management and search
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
HISTSIZE=10000
SAVEHIST=10000
bindkey '^[[A' history-beginning-search-backward
bindkey '^[[B' history-beginning-search-forward

# make PATH only accept unique values
typeset -U path

# add my local scripts and some homebrew pacakges use the sbin to PATH
path=(
	/usr/local/opt/gnu-getopt/bin
	/usr/local/opt/gnu-sed/libexec/gnubin
    ~/.local/bin
    /usr/local/sbin
	# binaries for go
	$(go env GOPATH)/bin
	# cargo binaries for rust
	~/.cargo/bin
	~/.rvm/bin
    $path
)
export PATH

# environment variables
source "$HOME/.env"

# zinit
source $(brew --prefix)/opt/zinit/zinit.zsh

# Plugins
zinit ice wait lucid
zinit light zsh-users/zsh-autosuggestions
zinit light zsh-users/zsh-syntax-highlighting

# prompt - oh-my-posh
eval "$(oh-my-posh init zsh --config $HOME/.config/oh-my-posh/main.toml)"

# SDKMAN (Disabled by default, enable if needed)
# [[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

# nodenv (Lazy Loading for Performance)
export NODENV_ROOT="$HOME/.nodenv"
if command -v nodenv &> /dev/null; then
  eval "$(nodenv init - --no-rehash)"
fi

# akamai completion (Lazy Loading)
# if [[ -x "/usr/local/bin/akamai" ]]; then
#   eval "$(/usr/local/bin/akamai --zsh)"
# fi

# thefuck (Lazy Loading)
if command -v thefuck &> /dev/null; then
  eval "$(thefuck --alias)"
fi

# fzf
source <(fzf --zsh)

# iterm2 integration (Lazy Loading)
if [[ -e "$HOME/.iterm2_shell_integration.zsh" ]]; then
  source "$HOME/.iterm2_shell_integration.zsh"
fi

# update netrc & gradle props (Run in Background to Avoid Delay)
# source "$HOME/.local/bin/generate_netrc" --force &> /dev/null &
# source "$HOME/.local/bin/generate_gradle_props" --force &> /dev/null &

# aliases
source "$HOME/.aliases"

# uncomment to debug slow zsh startup time
# zprof

if empty(glob('~/.cache/vim/tmp'))
    silent !mkdir -p ~/.cache/vim/tmp
endif
set directory=~/.cache/vim//
set backupdir=~/.cache/vim/tmp//
set undodir=~/.cache/vim/tmp//

set relativenumber
set showbreak=+++
set textwidth=100
set showmatch
set visualbell

set hlsearch
set smartcase
set ignorecase
set incsearch

set autoindent
set expandtab
set shiftwidth=2
set smartindent
set smarttab
set softtabstop=2
set syntax=on
set filetype=on
set ruler

set undolevels=1000
set backspace=indent,eol,start

